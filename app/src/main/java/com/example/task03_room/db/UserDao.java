package com.example.task03_room.db;

import android.widget.EditText;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    List<User> getAllUsers();

    @Query("SELECT * FROM user WHERE username = :userName")
    User getUser(String userName);

    @Query("SELECT * FROM user WHERE username = :userName AND password = :password")
    User getUserPass(String userName, String password);

    @Insert
    void insertUser(User... users);
}
