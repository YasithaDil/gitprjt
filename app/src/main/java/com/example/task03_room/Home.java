package com.example.task03_room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.task03_room.db.AppDatabase;
import com.example.task03_room.db.User;

import java.util.List;

public class Home extends AppCompatActivity {

    private Adapter adapter;
    Button profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Welcome message
        TextView welcome = (TextView) findViewById(R.id.welcome);
        Intent i = getIntent();
        Bundle b = i.getExtras();

        if(b!=null){
            String hello = (String) b.get("user");
            welcome.setText("Hello "+ hello);
        }

        //Initializing Recyclerview
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        adapter = new Adapter(this);
        recyclerView.setAdapter(adapter);

        //Fetching data from Database
        AppDatabase db = AppDatabase.getDbInstance(this.getApplicationContext());
        List<User> userList = db.userDao().getAllUsers();
        adapter.setUserList(userList);

    }
}