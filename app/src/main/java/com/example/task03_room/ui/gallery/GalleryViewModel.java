package com.example.task03_room.ui.gallery;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.task03_room.Adapter;

public class GalleryViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private Adapter adapter;

    public GalleryViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is gallery fragment");


    }

    public LiveData<String> getText() {
        return mText;
    }
}